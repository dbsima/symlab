# symlab

Playing with mathematically generated symbols using cool tools like
[Rough.js](https://roughjs.com/).


### How to clean up an image

1. Download the png
2. convert output.png -trim +repage -resize '128x128>' -gravity center -background transparent -extent 128x128 fav.png
